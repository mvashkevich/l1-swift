//
//  AuthorsStore.swift
//  BookWriters
//
//  Created by Admin on 02.04.2021.
//

import Foundation
import Combine
import SwiftUI

class AuthorsStore: ObservableObject {
    private var areAuthorsLoaded = false
    @Published var authors: [Author] = []
    
    private var authorsService: AuthorsService
    
    init(authorsService: AuthorsService) {
        self.authorsService = authorsService
    }
    
    func loadAllAuthors() {
        if !areAuthorsLoaded {
            areAuthorsLoaded = true
            authorsService.loadAllAuthors { authors, error in
                DispatchQueue.main.async {
                    self.authors = authors
                }
            }
        }
    }
    
    func resetAreAuthorsLoaded() {
        areAuthorsLoaded = false
    }
}
