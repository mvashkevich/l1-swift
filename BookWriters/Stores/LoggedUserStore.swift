//
//  LoggedUserStore.swift
//  BookWriters
//
//  Created by Admin on 02.04.2021.
//

import Combine
import Foundation

class LoggedUserStore: ObservableObject {
    let userIdKey = "user_id"
    
    @Published var userId: String {
        didSet {
            UserDefaults.standard.set(userId, forKey: userIdKey)
        }
    }
    
    init() {
        userId = UserDefaults.standard.string(forKey: userIdKey) ?? ""
    }
    
    func logOut() {
        userId = ""
    }
}

//For preview
func getResetLoggedUserStore() -> LoggedUserStore {
    let loggedUserStore = LoggedUserStore()
    loggedUserStore.userId = ""
    return loggedUserStore
}
