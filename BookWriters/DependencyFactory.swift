//
//  DependencyFactory.swift
//  BookWriters
//
//  Created by Admin on 06.04.2021.
//


import Foundation

class DependencyFactory {
    static let shared = DependencyFactory()
    
    private let authenticationService = FirebaseAuthenticationService()
    private let loggedUserStore = LoggedUserStore()
    private let settingsStore = SettingsStore()
    private let authorsRepository = FirebaseAuthorsRepository()
    private let mediaRepository = FirebaseMediaRepository()
    private let authorsStore: AuthorsStore
    
    init() {
        authorsStore = AuthorsStore(authorsService: FirebaseAuthorsService(authorsRepository: authorsRepository, mediaRepository: mediaRepository))
    }
    
    func getSettingsStore() -> SettingsStore {
        return settingsStore
    }
    
    func getLoggedUserStore() -> LoggedUserStore {
        return loggedUserStore
    }
    
    func getSignInViewModel() -> SignInViewModel {
        return SignInViewModel(authenticationService, loggedUserStore)
    }
    
    func getSignUpViewModel() -> SignUpViewModel {
        return SignUpViewModel(authenticationService, authorsRepository, mediaRepository, loggedUserStore)
    }
    
    func getAuthorsStore() -> AuthorsStore {
        return authorsStore
    }
    
    func getAuthorDetailsViewModel() -> AuthorDetailsViewModel {
        return AuthorDetailsViewModel(mediaRepository: mediaRepository)
    }
    
    func getAuthorEditViewModel() -> AuthorEditViewModel {
        return AuthorEditViewModel(authorsRepository: authorsRepository, mediaRepository: mediaRepository)
    }
}
