//
//  BookWritersApp.swift
//  BookWriters
//
//  Created by Admin on 3/21/21.
//

import SwiftUI
import Firebase
import GoogleMaps

@main
struct BookWritersApp: App {
    @UIApplicationDelegateAdaptor(AppDelegate.self) var appDelegate
    var body: some Scene {
        WindowGroup {
            ContentView()
                .environmentObject(DependencyFactory.shared.getSettingsStore())
                .environmentObject(DependencyFactory.shared.getLoggedUserStore())
                .environmentObject(DependencyFactory.shared.getAuthorsStore())
        }
    }
}

class AppDelegate: NSObject, UIApplicationDelegate {
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
        FirebaseApp.configure()
        GMSServices.provideAPIKey("AIzaSyBHjjKjnUJV0tPyPLZMdMdIVmfgWThdbCg")
        
        return true
    }
}
