//
//  Author.swift
//  BookWriters
//
//  Created by Admin on 3/21/21.
//

import SwiftUI
import FirebaseFirestoreSwift
import MapKit

struct Author: Identifiable, Codable {
    enum Sex: String, CaseIterable, Codable {
        case man = "Man"
        case woman = "Woman"
    }
    
    private enum CodingKeys: String, CodingKey {
        case id, email, password, login, birthDate, name, preferedLanguage, sex, country, almaMater, company, socialNetworkLink, conferenceVideoUrl, imageIds, location
    }
    
    @DocumentID var id = UUID().uuidString
    
    var email: String
    var password: String
    var login: String
    
    var birthDate: Date
    var name: String
    var preferedLanguage: String
    var sex: Sex
    var country: String
    var almaMater: String
    var company: String
    var socialNetworkLink: String
    
    var conferenceVideoUrl: URL?
    
    var imageIds: [String] = []
    var images: [UIImage]?
    
    var location: CLLocationCoordinate2D?
    
    mutating func update(author: Author) {
        self = author
    }
    
    static var testAuthor: Author {
        get {
            return Author(email: "mail@mail.com", password: "pwd", login: "", birthDate: Date(), name: "name",  preferedLanguage: "c", sex: Author.Sex.man, country: "Belarus", almaMater: "", company: "", socialNetworkLink: "")
        }
    }
}

extension CLLocationCoordinate2D: Codable {
    public func encode(to encoder: Encoder) throws {
        var container = encoder.unkeyedContainer()
        try container.encode(latitude)
        try container.encode(longitude)
    }
    
    public init(from decoder: Decoder) throws {
        var container = try decoder.unkeyedContainer()
        let latitude = try container.decode(CLLocationDegrees.self)
        let longitude = try container.decode(CLLocationDegrees.self)
        self.init(latitude: latitude, longitude: longitude)
    }
}

func getAuthorForPreview() -> Author {
    return Author.testAuthor
}
