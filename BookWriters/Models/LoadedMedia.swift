//
//  LoadedMedia.swift
//  BookWriters
//
//  Created by Admin on 31.03.2021.
//

import Foundation

struct LoadedMedia {
    var id: String
    var data: Data
    
    init(_ id: String, _ data: Data) {
        self.id = id
        self.data = data
    }
}
