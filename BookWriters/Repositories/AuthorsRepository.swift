//
//  AuthorsRepository.swift
//  BookWriters
//
//  Created by Admin on 31.03.2021.
//

import Foundation
import FirebaseFirestore
import FirebaseFirestoreSwift

protocol AuthorsRepository {
    func getAll(completion: @escaping ([Author]?, Error?) -> Void)
    func add(_ author: Author, completion: @escaping (Error?) -> Void)
    func update(_ author: Author, completion: @escaping (Error?) -> Void)
}

class FirebaseAuthorsRepository: AuthorsRepository {
    func update(_ author: Author, completion: @escaping (Error?) -> Void) {
        do {
            _ = try authorsRef.document(author.id!).setData(from: author)
            completion(nil)
        } catch {
            completion(error)
        }
    }
    
    func getAll(completion: @escaping ([Author]?, Error?) -> Void) {
        authorsRef.getDocuments() { querySnapshot, error in
            if let error = error {
                completion(nil, error)
                return
            }
            
            var authors = querySnapshot?.documents.compactMap() {document in
                try? document.data(as: Author.self)} ?? []
            authors.sort(by: { $0.name < $1.name})
            completion(authors, nil)
        }
    }
    
    func add(_ author: Author, completion: @escaping (Error?) -> Void) {
        do {
            _ = try authorsRef.document(author.id!).setData(from: author)
            completion(nil)
        } catch {
            completion(error)
        }
    }
    
    private let path = "authors"
    
    private lazy var authorsRef = Firestore.firestore().collection(path)
}
