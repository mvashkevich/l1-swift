//
//  AuthorsService.swift
//  BookWriters
//
//  Created by Admin on 3/21/21.
//

import Foundation
import SwiftUI

protocol AuthorsService {
    func loadAllAuthors(completion: @escaping ([Author], Error?) -> Void)
}

class FirebaseAuthorsService: AuthorsService {
    func loadAllAuthors(completion: @escaping ([Author], Error?) -> Void) {
        authorsRepository.getAll(completion: { authors, error in
            if error != nil {
                completion([], error)
                return
            }
            
            completion(authors ?? [], nil)
        })
    }
    
    private let authorsRepository: AuthorsRepository
    private let mediaRepository: MediaRepository
    
    init(authorsRepository: AuthorsRepository, mediaRepository: MediaRepository) {
        self.authorsRepository = authorsRepository
        self.mediaRepository = mediaRepository
    }
    
}
