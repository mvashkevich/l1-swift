//
//  ImageWithText.swift
//  BookWriters
//
//  Created by Admin on 3/5/21.

//

import SwiftUI

struct ImageWithText: View {
    private var imageName: String
    private var text: String
    
    init(_ imageName: String, _ text: String) {
        self.imageName = imageName
        self.text = text
    }
    
    var body: some View {
        HStack {
            Image(imageName)
                .resizable()
                .interpolation(.none)
                .frame(width: 16, height: 16)
            Text(LocalizedStringKey(text))
        }
    }
}

struct ImageWithText_Previews: PreviewProvider {
    static var previews: some View {
        Form {
            Picker(selection: .constant("Creeper"), label: Text("Occupation")) {
                ImageWithText("Creeper", "Creeper")
                    .tag("Creeper")
            }
        }
    }
}
