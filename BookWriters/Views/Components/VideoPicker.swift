//
//  VideoPicker.swift
//  BookWriters
//
//  Created by Admin on 3/9/21.
//

import SwiftUI
import Foundation
import Firebase
import PhotosUI

struct VideoPicker: UIViewControllerRepresentable {
    @Environment(\.presentationMode) var presentationMode
    @Binding var videoURL: URL?
    @Binding var pickerResult: PHPickerResult?
    
    class Coordinator: NSObject, PHPickerViewControllerDelegate {
        func picker(_ picker: PHPickerViewController, didFinishPicking results: [PHPickerResult]) {
            DispatchQueue.main.async {
                if results.count > 0 {
                    self.parent.pickerResult = results[0]
                }
            }
            self.parent.presentationMode.wrappedValue.dismiss()
        }
        
        let parent: VideoPicker
        
        init(_ parent: VideoPicker) {
            self.parent = parent
        }
    }
    
    func makeCoordinator() -> Coordinator {
        Coordinator(self)
    }
    
    func makeUIViewController(context: Context) -> PHPickerViewController {
        var configuration = PHPickerConfiguration()
        configuration.filter = .videos
        configuration.preferredAssetRepresentationMode = .current
        
        let picker = PHPickerViewController(configuration: configuration)
        
        picker.delegate = context.coordinator
        
        return picker
    }
    
    func updateUIViewController(_ uiViewController: PHPickerViewController, context: Context) {
        
    }
}
