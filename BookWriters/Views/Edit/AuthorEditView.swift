//
//  AuthorEdit.swift
//  BookWriters
//
//  Created by Admin on 06.04.2021.
//

import SwiftUI

fileprivate enum SheetMode {
    case video, image
}

fileprivate class SheetConfig: ObservableObject {
    var mode: SheetMode = .image
    @Published var show = false
    
    func show(mode: SheetMode) {
        self.mode = mode
        self.show = true
    }
}

struct AuthorEditView: View {
    @Environment(\.presentationMode) var isShowingEdit
    @EnvironmentObject private var settings: SettingsStore
    @StateObject private var sheetConfig = SheetConfig()
    @ObservedObject var viewModel: AuthorEditViewModel
    
    let dateRange: ClosedRange<Date> = {
        let calendar = Calendar.current
        let startComponents = DateComponents(year: 1900, month: 1, day: 1)
        let today = Date()
        let endComponents = calendar.dateComponents([.year, .month, .day], from: today)
        return calendar.date(from:startComponents)!
            ...
            calendar.date(from:endComponents)!
    }()
    
    init(viewModel: AuthorEditViewModel, author: Binding<Author>) {
        self.viewModel = viewModel
        viewModel.onViewCreated(author: author)
    }
    
    var body: some View {
        NavigationView {
            List {
                Section(header: Text("Credentials")) {
                    TextField("Email address", text: $viewModel.email)
                        .disableAutocorrection(true)
                        .autocapitalization(.none)
                }
                Section(header: Text("Personal information")) {
                    TextField("Name", text: $viewModel.name)
                    DatePicker(
                        "Birth date",
                        selection: $viewModel.birthDate,
                        in: dateRange,
                        displayedComponents: [.date]
                    )
                    Picker(selection: $viewModel.sex, label: Text("Sex"), content: {
                        ForEach(Author.Sex.allCases, id: \.self.rawValue) {
                            Text($0.rawValue)
                        }
                    })
                    TextField("County", text: $viewModel.country)
                }
                Section(header: Text("Professional information")) {
                    TextField("Prefered language", text: $viewModel.preferedLanguage)
                    TextField("Alma mater", text: $viewModel.almaMater)
                    TextField("Company", text: $viewModel.company)
                    TextField("Social network link", text: $viewModel.socialNetworkLink)
                        .disableAutocorrection(true)
                        .autocapitalization(.none)
                }
                Section(
                    header:
                        HStack {
                            Text("Location")
                                .font(.custom(settings.fontName, size: settings.fontSize * 0.75))
                            Spacer()
                        }
                ) {
                    TextField("Latitude", text: $viewModel.latitude)
                    TextField("Longitude", text: $viewModel.longitude)
                }
                
                Section(header: Text("Images")) {
                    ForEach(0..<viewModel.displayedImages.count, id: \.self) { i in
                        Image(uiImage: viewModel.displayedImages[i].image)
                            .resizable()
                            .scaledToFill()
                            .frame(width: 90, height: 90)
                            .cornerRadius(7)
                    }
                    .onDelete { deletedImagesIndexes in
                        viewModel.handleRemoveImage(deletedImagesIndexes: deletedImagesIndexes)
                    }
                }
                
                Section{
                    HStack {
                        Spacer()
                        Button(action: { sheetConfig.show(mode: .image) }) {
                            Text("Add image")
                        }
                        Spacer()
                    }
                }
                
                Section {
                    HStack {
                        Spacer()
                        if ((viewModel.isAuthorHasSavedVideo && !viewModel.isSavedVideoShouldBeDeleted) || viewModel.pickerResult != nil) {
                            Button(action: viewModel.handleDeleteVideoButtonClick) {
                                Text("Delete video")
                            }
                        } else {
                            Button(action: { sheetConfig.show(mode: .video) }) {
                                Text("Add video")
                            }
                        }
                        Spacer()
                    }
                }
            }
            .listStyle(GroupedListStyle())
            .navigationBarTitle("Edit", displayMode: .inline)
            .navigationBarItems(
                leading:
                    Button(action: {                         isShowingEdit.wrappedValue.dismiss()
                    } ) {
                        Text("Cancel")
                    },
                trailing:
                    HStack {
                        if viewModel.isAuthorUpdating {
                            ProgressView()
                        } else {
                            Button(action: viewModel.handleUpdateAuthorButtonClick) {
                                Text("Save")
                            }
                        }
                    }
            )
        }
        .sheet(isPresented: $sheetConfig.show) {
            switch sheetConfig.mode {
            case .image:
                ImagePicker(callback: viewModel.addImage)
            case .video:
                VideoPicker(videoURL: $viewModel.newVideoLocalURL, pickerResult: $viewModel.pickerResult)
            }
        }
    }
}
