//
//  AuthorEditViewModel.swift
//  BookWriters
//
//  Created by Admin on 06.04.2021.
//

import SwiftUI
import FirebaseFirestore
import GoogleMaps
import PhotosUI

struct ImageData {
    var image: UIImage
    var imageId: String?
}

class AuthorEditViewModel: ObservableObject {    
    private var authorsRepository: AuthorsRepository
    private var mediaRepository: MediaRepository
    
    private var deletedImages: [String] = []
    
    private var author: Binding<Author>!
    
    @Environment(\.presentationMode) var isShowingEdit
    
    @Published var email: String = ""
    
    @Published var birthDate: Date = Date()
    @Published var name: String = ""
    @Published var preferedLanguage: String = ""
    @Published var sex: Author.Sex = .man
    @Published var country: String = ""
    @Published var almaMater: String = ""
    @Published var company: String = ""
    @Published var socialNetworkLink: String = ""
    
    @Published var latitude = ""
    @Published var longitude = ""
    
    @Published var displayedImages: [ImageData] = []
    @Published var newVideoLocalURL: URL?
    @Published var isSavedVideoShouldBeDeleted = false
    @Published var isAuthorHasSavedVideo = false
    @Published var isAuthorUpdating = false
    @Published var pickerResult: PHPickerResult?
    @Published var isSaved = false
    
    init(authorsRepository: AuthorsRepository, mediaRepository: MediaRepository) {
        self.authorsRepository = authorsRepository
        self.mediaRepository = mediaRepository
    }
    
    func onViewCreated(author: Binding<Author>) {
        self.author = author
        
        isAuthorHasSavedVideo = author.wrappedValue.conferenceVideoUrl != nil
        initializeState()
    }
    
    func handleUpdateAuthorButtonClick() {
        isAuthorUpdating = true
        
        let imagesToLoadDataList = displayedImages
            .filter { $0.imageId == nil }
            .map { $0.image.jpegData(compressionQuality: 1)! }
        
        mediaRepository.deleteUserImages(userId: author.wrappedValue.id!, imageIds: deletedImages) { [self] error in
            if handleCallbackError(error) { return }
            
            mediaRepository.uploadUserImages(userId: author.wrappedValue.id!, imagesData: imagesToLoadDataList){ (newImagesIds, error) in
                if handleCallbackError(error) { return }
                
                fillMissingIds(newImagesIds: newImagesIds)
                
                if isSavedVideoShouldBeDeleted {
                    mediaRepository.deleteVideo(videoUrl: author.wrappedValue.conferenceVideoUrl!) { error in
                        if handleCallbackError(error) { return }
                        uploadVideoAndUpdateAuthorData()
                    }
                } else {
                    uploadVideoAndUpdateAuthorData()
                }
            }
        }
    }
    
    private func fillMissingIds(newImagesIds: [String]){
        var currentImageIdIndex = 0
        for i in 0..<displayedImages.count {
            if displayedImages[i].imageId == nil {
                displayedImages[i].imageId = newImagesIds[currentImageIdIndex]
                currentImageIdIndex += 1
            }
        }
    }
    
    private func uploadVideoAndUpdateAuthorData(){
        if (pickerResult != nil) {
            if let itemProvider = pickerResult?.itemProvider {
                
                if itemProvider.hasItemConformingToTypeIdentifier(AVFileType.mp4.rawValue) {
                    itemProvider.loadDataRepresentation(forTypeIdentifier: AVFileType.mp4.rawValue) { [self]
                        (data, error) in
                        
                        guard let videoData = data else { return }
                        
                        mediaRepository.uploadUserVideo(userId: author.wrappedValue.id!, videoData: videoData) { [self] videoUrl, error in
                            if handleCallbackError(error) { return }
                            
                            saveUpdatedAuthor(newVideoUrl: videoUrl)
                            
                        }}}
            }}
        else {
            saveUpdatedAuthor(newVideoUrl: nil)
        }
    }
    
    private func saveUpdatedAuthor(newVideoUrl: URL?){
        let newAuthor = createUpdatedAuthor(newVideoUrl: newVideoUrl)
        
        authorsRepository.update(newAuthor) { [self] error in
            if handleCallbackError(error) { return }
            
            DispatchQueue.main.async {
                author.wrappedValue.update(author: newAuthor)
                
                print("Author has been updated")
                isSaved = true
                resetFields()
                isAuthorUpdating = false
            }
        }
    }
    
    private func createUpdatedAuthor(newVideoUrl: URL?) -> Author {
        let imageIds = displayedImages.map { $0.imageId! }
        let images = displayedImages.map { $0.image }
        let location = (latitude != "" && longitude != "") ?
            CLLocationCoordinate2D(latitude: Double(latitude)!, longitude: Double(longitude)!) : nil
        let videoUrl = (isSavedVideoShouldBeDeleted || author.wrappedValue.conferenceVideoUrl == nil) ? newVideoUrl : author.wrappedValue.conferenceVideoUrl
        
        return Author(id: author.wrappedValue.id, email: author.wrappedValue.email, password: author.wrappedValue.password, login: author.wrappedValue.login, birthDate: author.wrappedValue.birthDate, name: author.wrappedValue.name, preferedLanguage: author.wrappedValue.preferedLanguage, sex: author.wrappedValue.sex, country: author.wrappedValue.country, almaMater: author.wrappedValue.almaMater, company: author.wrappedValue.company, socialNetworkLink: author.wrappedValue.socialNetworkLink, conferenceVideoUrl: videoUrl, 	imageIds: imageIds, images: images, location: location)
    }
    
    
    func handleRemoveImage(deletedImagesIndexes: IndexSet) {
        deletedImagesIndexes.forEach { deletedImageIndex in
            let imgData = displayedImages[deletedImageIndex]
            if let id = imgData.imageId {
                deletedImages.append(id)
            }
        }
        displayedImages.remove(atOffsets: deletedImagesIndexes)
    }
    
    func addImage(_ uiImage: UIImage) {
        displayedImages.append(ImageData(image: uiImage, imageId: nil))
    }
    
    func handleDeleteVideoButtonClick() {
        newVideoLocalURL = nil
        
        if author.wrappedValue.conferenceVideoUrl != nil {
            isSavedVideoShouldBeDeleted = true
        }
    }
    
    private func initializeState() {
        email = author.wrappedValue.email
        birthDate = author.wrappedValue.birthDate
        name = author.wrappedValue.name
        preferedLanguage = author.wrappedValue.preferedLanguage
        sex = author.wrappedValue.sex
        country = author.wrappedValue.country
        almaMater = author.wrappedValue.almaMater
        company = author.wrappedValue.company
        socialNetworkLink = author.wrappedValue.socialNetworkLink
        displayedImages = (0..<author.wrappedValue.imageIds.count).map { ImageData(image: author.wrappedValue.images![$0], imageId: author.wrappedValue.imageIds[$0]) }
        if let location = author.wrappedValue.location {
            latitude = String(location.latitude)
            longitude = String(location.longitude)
        }
    }
    
    private func resetFields() {
        isAuthorHasSavedVideo = author.wrappedValue.conferenceVideoUrl != nil
        deletedImages = []
        isSavedVideoShouldBeDeleted = false
        newVideoLocalURL = nil
    }
    
    private func handleCallbackError(_ error: Error?) -> Bool{
        if let error = error {
            DispatchQueue.main.async {
                print(error.localizedDescription)
                self.resetFields()
                self.isAuthorUpdating = false
            }
            return true
        }
        return false
    }
}
