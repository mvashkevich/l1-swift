//
//  AuthorGridView.swift
//  BookWriters
//
//  Created by Admin on 3/21/21.
//

import SwiftUI

struct AuthorGridView: View {
    
    
    @EnvironmentObject private var settings: SettingsStore
    @EnvironmentObject private var authorsStore: AuthorsStore
    
    var body: some View {
        NavigationView {
            List {
                ForEach(0..<authorsStore.authors.count, id: \.self) { index in
                    NavigationLink(destination: AuthorDetails(viewModel: DependencyFactory.shared.getAuthorDetailsViewModel(), author: $authorsStore.authors[index])) {
                        AuthorGridElement(author: authorsStore.authors[index])
                            .accentColor(settings.isDarkMode ? .white : .black)
                    }
                    
                }
                .padding(.horizontal)
                
            }
        }
    }
}

struct AuthorGridView_Previews: PreviewProvider {
    static var previews: some View {
        AuthorGridView()
    }
}
