//
//  AuthorDetails.swift
//  BookWriters
//
//  Created by Admin on 3/21/21.
//

import SwiftUI
import AVKit

fileprivate struct DetailRow<T>: View where T: View {
    var leftText: LocalizedStringKey
    var rightView: T
    
    init(_ leftText: LocalizedStringKey, _ rightView: T) {
        self.leftText = leftText
        self.rightView = rightView
    }
    
    var body: some View {
        HStack(alignment: .top) {
            Text(leftText)
                .bold()
            Spacer()
            rightView
        }
        .padding(.horizontal)
        .padding(.vertical, 10)
    }
}



struct AuthorDetails: View {
    static let previewImageCount = 3
    let formatter: DateFormatter
    let layout = Array(repeating: GridItem(.flexible()), count: previewImageCount + 1)
    
    @EnvironmentObject private var settings: SettingsStore
    @State private var isShowingEditView = false
    @ObservedObject var viewModel: AuthorDetailsViewModel
    @Binding var author: Author
    
    init(viewModel: AuthorDetailsViewModel, author: Binding<Author>) {
        self.viewModel = viewModel
        self._author = author
        self.formatter = DateFormatter()
        self.formatter.dateStyle = .short
        viewModel.setAuthor(Author: author)
    }
    
    var body: some View {
        ScrollView {
            Text(author.name)
                .font(.title)
            Divider()
            VStack(alignment: .leading) {
                Section(header: Text("Personal")) {
                    let date = formatter.string(from: author.birthDate)
                    DetailRow("Birth date", Text(date))
                    DetailRow("Email", Text(author.email))
                    DetailRow("Sex", Text(author.sex.rawValue))
                    DetailRow("Country", Text(author.country))
                }
                Section(header: Text("Professional")) {
                    DetailRow("Language", Text(author.preferedLanguage))
                    DetailRow("Alma mater", Text(author.almaMater))
                    
                    if let url = URL(string: author.socialNetworkLink) {
                        DetailRow("Social network link", Link(url.host!, destination: url))
                    }
                }
                if let location = author.location {
                    Section(header: Text("Location")) {
                        DetailRow("Latitude", Text("\(location.latitude)"))
                        DetailRow("Longitude", Text("\(location.longitude)"))
                    }
                }
                if author.imageIds.count != 0 {
                    Section(header: Text("Books")) {
                        if let images = author.images {
                            LazyVGrid(columns: layout) {
                                ForEach(0..<images.prefix(AuthorDetails.previewImageCount).count, id: \.self) { index in
                                    NavigationLink(destination: ImageGallery(images: author.images!, selectedTab: index)) {
                                        Image(uiImage: images[index])
                                            .resizable()
                                            .aspectRatio(contentMode: .fill)
                                            .frame(height: 50)
                                            .clipped()
                                            .cornerRadius(3)
                                    }
                                }
                                NavigationLink(destination: GridImageGallery(images: author.images!)) {
                                    Text("View all")
                                        .frame(maxWidth: .infinity, maxHeight: .infinity)
                                }
                            }
                            .padding(.top, 10)
                        } else {
                            HStack {
                                Spacer()
                                ProgressView()
                                Spacer()
                            }
                        }
                    }
                }
                if let videoUrl = author.conferenceVideoUrl {
                    Section(header: Text("Conference video")) {
                        VideoPlayer(player: AVPlayer(url: videoUrl))
                            .frame(height: 200)
                    }
                    .padding(.top, 10)
                }
            }
        }
        .padding()
        .navigationBarItems(
            trailing:
                Button(action: {isShowingEditView.toggle()}) {
                    Text("Edit")
                }
                .disabled(!author.imageIds.isEmpty && author.images == nil))
        .navigationBarTitle(author.name, displayMode: .inline)
        .fullScreenCover(isPresented: $isShowingEditView) {
            AuthorEditView(viewModel: DependencyFactory.shared.getAuthorEditViewModel(), author: _author)
                .environmentObject(settings)
                .environment(\.colorScheme, settings.colorScheme)
                .accentColor(settings.color)
        }
        .onAppear(perform: viewModel.loadAuthorGallery)
    }
}
