//
//  AuthorDetailsViewModel.swift
//  BookWriters
//
//  Created by Admin on 05.04.2021.
//

import Foundation
import Combine
import SwiftUI

class AuthorDetailsViewModel: ObservableObject {
    @Published var Author: Binding<Author>!
    
    private let mediaRepository: MediaRepository
    
    internal init(mediaRepository: MediaRepository) {
        self.mediaRepository = mediaRepository
    }
    
    func setAuthor(Author: Binding<Author>) {
        self.Author = Author
    }
    
    func loadAuthorGallery() {
        if Author.images.wrappedValue == nil {
            if Author.imageIds.wrappedValue.isEmpty {
                Author.images.wrappedValue = []
                return
            }
            
            mediaRepository.loadUserGallery(userId: Author.id.wrappedValue!, imageIds: Author.imageIds.wrappedValue) { loadedImages, error in
                let images = loadedImages.map { UIImage(data: $0.data)! }
                DispatchQueue.main.async {
                    self.Author.images.wrappedValue = images
                }
            }
        }
    }
}
