//
//  MainView.swift
//  BookWriters
//
//  Created by Admin on 3/21/21.
//

import SwiftUI

struct MainView: View {
    @State private var selectedTab = 0
    @EnvironmentObject private var authorsStore: AuthorsStore
    @EnvironmentObject private var settings: SettingsStore
    
    var body: some View {
        NavigationView {
            TabView(selection: $selectedTab) {
                AuthorGridView()
                    .tabItem {
                        Label("Authors", systemImage: "person")
                    }
                    .tag(0)
                    .navigationBarHidden(true)
                
                MapView()
                    .tabItem {
                        Label("Map", systemImage: "map")
                    }
                    .tag(1)
                    .navigationBarHidden(true)
                
                SettingsView()
                    .tabItem {
                        Label("Settings", systemImage: "gear")
                    }
                    .tag(2)
                    .navigationBarHidden(true)
                
            }
            .ignoresSafeArea(edges: .bottom)
            .onAppear() {
                authorsStore.loadAllAuthors()
            }
        }
        .navigationViewStyle(StackNavigationViewStyle())
    }
}

struct MainView_Previews: PreviewProvider {
    static var previews: some View {
        MainView()
    }
}
