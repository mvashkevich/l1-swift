//
//  SignInViewModel.swift
//  BookWriters
//
//  Created by Admin on 06.04.2021.
//

import Foundation
import Combine

class SignInViewModel: ObservableObject {
    private var authenticationService: AuthenticationService
    private var loggedUserStore: LoggedUserStore
    
    @Published var email = ""
    @Published var password = ""
    @Published var showAlert = false
    @Published var showSignUpView = false
    var errorMessage = ""
    
    init(_ authenticationService: AuthenticationService, _ loggedUserStore: LoggedUserStore) {
        self.authenticationService = authenticationService
        self.loggedUserStore = loggedUserStore
    }
    
    func signIn() {
        authenticationService.signInWithEmail(email: email, password: password) { error, userId in
            if let error = error {
                DispatchQueue.main.async {
                    self.errorMessage = error.localizedDescription
                    self.showAlert = true
                }
                return
            }
            
            if let userId = userId {
                DispatchQueue.main.async {
                    self.loggedUserStore.userId = userId
                }
            }
        }
    }
}
