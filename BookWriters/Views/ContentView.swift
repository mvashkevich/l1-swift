//
//  ContentView.swift
//  BookWriters
//
//  Created by Admin on 3/21/21.
//

import SwiftUI
import Combine
import Firebase
import FirebaseAuth

struct ContentView: View {
    @EnvironmentObject private var loggedUserStore: LoggedUserStore
    @EnvironmentObject private var settings: SettingsStore
    
    var body: some View {
        VStack {
            if loggedUserStore.userId.isEmpty {
                SignInView(viewModel: DependencyFactory.shared.getSignInViewModel())
            }
            else {
                MainView()
            }
        }
        .animation(.spring())
        .environment(\.colorScheme, settings.colorScheme)
        .font(.custom(settings.fontName, size: settings.fontSize))
        .accentColor(settings.color)
        .environment(\.locale, .init(identifier: settings.locale))
    }
}


struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
