//
//  MapView.swift
//  BookWriters
//
//  Created by Admin on 3/21/21.
//

import SwiftUI
import GoogleMaps

struct MapView: UIViewRepresentable {
    @EnvironmentObject private var authorsStore: AuthorsStore
    func makeUIView(context: Context) -> GMSMapView {
        return GMSMapView.map(withFrame: .zero, camera: GMSCameraPosition.camera(withLatitude: 52, longitude: 30, zoom: 6.0))
    }
    
    func updateUIView(_ uiView: GMSMapView, context: Context) {
        for author in authorsStore.authors.filter({$0.location != nil}) {
            let marker = GMSMarker()
            marker.position = author.location!
            marker.title = author.name
            marker.map = uiView
        }
    }
}
//
//class Delegate: UIViewController, GMSMapViewDelegate {
//
//    let mapView = GMSMapView.map(withFrame: .zero, camera: GMSCameraPosition.camera(withLatitude: 52, longitude: 30, zoom: 6.0))
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        mapView.delegate = self
//    }
//
//    override func viewDidAppear(_ animated: Bool) {
//        for author in authorsStore.authors.filter({$0.location != nil}) {
//            let marker = GMSMarker()
//            marker.position = author.location!
//            marker.title = author.name
//            marker.map = mapView
//        }
//    }
//}
//
//class MapView: UIViewRepresentable {
//    let delegate = Delegate()
//
//    func makeUIView(context: Context) -> GMSMapView {
//        return delegate.mapView
//    }
//
//    func updateUIView(_ uiView: GMSMapView, context: Context) {
//
//    }
//
//    typealias UIViewType = GMSMapView
//}
