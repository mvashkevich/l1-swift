//
//  SettingsView.swift
//  BookWriters
//
//  Created by Admin on 3/21/21.
//

import SwiftUI
struct SettingsView: View {
    private let fontNames = ["Helvetica", "Avenir Next", "Noteworthy", "Papyrus", "Chalkboard SE"]
    
    @EnvironmentObject private var settings: SettingsStore
    @EnvironmentObject private var loggedUserStore: LoggedUserStore
    @EnvironmentObject private var authorsStore: AuthorsStore
    
    var body: some View {
        Form {
            Picker(
                selection: $settings.locale,
                label: Text("Language")
            ) {
                Text("English").tag("en")
                Text("Русский").tag("ru")
            }
            
            Picker(
                selection: $settings.fontName,
                label: Text("Font name")
            ) {
                ForEach(0..<fontNames.count) { index in
                    Text(fontNames[index])
                        .font(.custom(fontNames[index], size: CGFloat(settings.fontSize)))
                        .tag(fontNames[index])
                }
            }
            
            HStack {
                Text("Font size")
                Spacer(minLength: 40)
                Slider(value: $settings.fontSize, in: 10...25)
            }
            
            ColorPicker("Color", selection: $settings.color)
            
            Toggle(isOn: $settings.isDarkMode) {
                Text("Dark mode")
            }
            
            Button(action: {
                self.settings.resetDefaults()
            }) {
                Text("Reset settings")
            }
            
            Section {
                Button(action: {
                    authorsStore.resetAreAuthorsLoaded()
                    loggedUserStore.logOut()
                }) {
                    HStack {
                        Spacer()
                        Text("Log out")
                        Spacer()
                    }
                }
            }
        }
    }
}

struct SettingsView_Previews: PreviewProvider {
    static var previews: some View {
        SettingsView()
    }
}
