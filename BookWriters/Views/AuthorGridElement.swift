//
//  AuthorGridElement.swift
//  BookWriters
//
//  Created by Admin on 3/21/21.
//

import SwiftUI

struct AuthorGridElement: View {
    @EnvironmentObject var settings: SettingsStore
    
    var author: Author
    
    var body: some View {
        VStack(alignment: .leading, spacing: 3) {
                Text(author.name)
                    .font(.headline)
                    .lineLimit(1)
                Text(author.preferedLanguage)
                    .font(.subheadline)
                    .lineLimit(1)
                Text(author.almaMater)
                    .font(.subheadline)
                    .lineLimit(1)
                Text(author.country)
                    .font(.subheadline)
                    .lineLimit(1)
            }
            .padding(10)
        }
}

struct AuthorGridElement_Previews: PreviewProvider {
    static var previews: some View {
        AuthorGridElement(author: Author.testAuthor)
    }
}
