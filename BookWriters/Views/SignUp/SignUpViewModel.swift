//
//  SignUpViewModel.swift
//  BookWriters
//
//  Created by Admin on 06.04.2021.
//


import Foundation
import Combine
import UIKit
import SwiftUI

class SignUpViewModel: ObservableObject {
    private var authenticationService: AuthenticationService
    private var authorsRepository: AuthorsRepository
    private var mediaRepository: MediaRepository
    private var loggedUserStore: LoggedUserStore
    
    @Published var email: String = ""
    @Published var password: String = ""
    @Published var login: String = ""
    
    @Published var birthDate: Date = Date()
    @Published var name: String = ""
    @Published var preferedLanguage: String = ""
    @Published var sex: Author.Sex = .man
    @Published var country: String = ""
    @Published var almaMater: String = ""
    @Published var company: String = ""
    @Published var socialNetworkLink: String = ""
    
    @Published var isSigningUp = false
    @Published var showAlert = false
    var errorMessage = ""
    
    internal init(_ authenticationService: AuthenticationService, _ authorsRepository: AuthorsRepository, _ mediaRepository: MediaRepository, _ loggedUserStore: LoggedUserStore) {
        self.authenticationService = authenticationService
        self.authorsRepository = authorsRepository
        self.mediaRepository = mediaRepository
        self.loggedUserStore = loggedUserStore
        
    }
    
    func signUp() {
        isSigningUp = true
        
        authenticationService.signUpWithEmail(email: email, password: password) { [self] error, userId  in
            if handleError(error: error) {
                return
            }
            
            guard let userId = userId else {
                return
            }
            
            let author = Author(id: userId, email: email, password: password, login: login, birthDate: birthDate, name: name, preferedLanguage: preferedLanguage, sex: sex, country: country, almaMater: almaMater, company: company, socialNetworkLink: socialNetworkLink)
            
            authorsRepository.add(author) { error in
                if handleError(error: error) {
                    return
                }
                
                DispatchQueue.main.async {
                    self.loggedUserStore.userId = userId
                }
            }
        }
    }
    
    
    private func handleError(error: Error?) -> Bool {
        if let error = error {
            DispatchQueue.main.async {
                self.errorMessage = error.localizedDescription
                self.showAlert = true
                self.isSigningUp = false
            }
            return true
        }
        
        return false
    }
}
