//
//  SignUpView.swift
//  BookWriters
//
//  Created by Admin on 3/21/21.
//

import SwiftUI

struct SignUpView: View {
    @EnvironmentObject private var settings: SettingsStore
    @ObservedObject var viewModel: SignUpViewModel
    
    let dateRange: ClosedRange<Date> = {
        let calendar = Calendar.current
        let startComponents = DateComponents(year: 1900, month: 1, day: 1)
        let today = Date()
        let endComponents = calendar.dateComponents([.year, .month, .day], from: today)
        return calendar.date(from:startComponents)!
            ...
            calendar.date(from:endComponents)!
    }()
    
    var body: some View {
        NavigationView {
            List {
                Section(header: Text("Credentials")) {
                    TextField("Email address", text: $viewModel.email)
                        .disableAutocorrection(true)
                        .autocapitalization(.none)
                    TextField("Password", text: $viewModel.password)
                        .disableAutocorrection(true)
                        .autocapitalization(.none)
                    TextField("Login", text: $viewModel.login)
                        .disableAutocorrection(true)
                        .autocapitalization(.none)
                }
                Section(header: Text("Personal information")) {
                    TextField("Name", text: $viewModel.name)
                    DatePicker(
                        "Birth date",
                        selection: $viewModel.birthDate,
                        in: dateRange,
                        displayedComponents: [.date]
                    )
                    Picker(selection: $viewModel.sex, label: Text("Sex"), content: {
                        ForEach(Author.Sex.allCases, id: \.self.rawValue) {
                            Text($0.rawValue)
                        }
                    })
                    TextField("Country", text: $viewModel.country)
                }
                Section(header: Text("Professional information")) {
                    TextField("Prefered language", text: $viewModel.preferedLanguage)
                    TextField("Alma mater", text: $viewModel.almaMater)
                    TextField("Company", text: $viewModel.company)
                    TextField("Social network link", text: $viewModel.socialNetworkLink)
                        .disableAutocorrection(true)
                        .autocapitalization(.none)
                }
                Section{
                    HStack {
                        Spacer()
                        if viewModel.isSigningUp {
                            ProgressView()
                        } else {
                            Button(action: viewModel.signUp) {
                                Text("Sign Up")
                            }
                        }
                        Spacer()
                    }
                }
            }
            .listStyle(GroupedListStyle())
            .navigationBarTitle("Sign up", displayMode: .inline)
        }
    }
}

